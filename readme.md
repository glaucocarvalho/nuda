# NUDA - Nubank Data Analytics

The main goal of this project is to extract data from previous credit card bills and
get relevant dat from customer behavior

## Installation
```bash
pip install -r requirements.txt
```

## commands
Sure that is a less bazookish way to handle these commands, but i like this way :).

extract data from nubank.csv files stored in /files
```bash
./manage.py extract
```
Command used to test data filtering in andy_panda.main()
```bash
./manage.py panda
```

## Running Django server:
```bash
./manage.py runserver 
```

## Roadmap
* [x] step 1. data extractor
* [x] step 2. data analysis
* [x] step 3. rest api
* [ ] step 4. angular frontend
* [ ] step 5. dynamic graphs

### step 2. Target Features
* billing month histogram [ok]
* top spending by period [ok]
* top spending categories by period [ok]
* most expensive months [ok]
* trace purchases occurrences by month [ok]

### step 3. Api
API docs can be found in http://127.0.0.1:8000/api/swagger/.

## Apps tree

├── Nuda
│   ├── Main Django app.
│   ├── DataXtractor 
│   │   ├── App to extract data and save it to sqlite in Nuda.
│   ├── Nuda
│   │   ├── App with Django config.
│   ├── Pandarem
│   │   ├── App with panda logic to process extracted data.
│   ├── RestAPI
│   │   ├── App with a rest server serving Pandarem's processed data.
├── Mitrhandir
│   ├── Main Andular app.

## Future work:
* Change "for's" in andy_panda.py to data frame manipulation should improve performance
