asgiref==3.5.2
certifi==2022.12.7
charset-normalizer==2.1.1
coreapi==2.3.3
coreschema==0.0.4
Django==4.1.4
djangorestframework==3.14.0
drf-yasg==1.21.4
ftfy==6.1.1
idna==3.4
inflection==0.5.1
itypes==1.2.0
Jinja2==3.1.2
MarkupSafe==2.1.1
numpy==1.23.5
openapi-codec==1.3.2
packaging==22.0
pandas==1.5.2
python-dateutil==2.8.2
pytz==2022.6
requests==2.28.1
ruamel.yaml==0.17.21
scipy==1.9.3
simplejson==3.18.0
six==1.16.0
sqlparse==0.4.3
tzdata==2022.7
Unidecode==1.3.6
uritemplate==4.1.1
urllib3==1.26.13
wcwidth==0.2.5
