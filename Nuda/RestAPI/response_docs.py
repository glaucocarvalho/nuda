from drf_yasg import openapi

GetHistogram_response = {
    "200": openapi.Response(
        description="OK",
        examples={
            "application/json": {
                "histogram": {
                    "yearmonth": {
                        "amount": "value"
                    },
                    "202211": {
                        "amount": 5000
                    },
                    "202212": {
                        "amount": 6000
                    },
                }
            }
        }
    ),
}

GetBillsFromDate_response = {
    "200": openapi.Response(
        description="OK",
        examples={
            "application/json": {
                "bills": [{
                    "date": "2020-12-14",
                    "category": "restaurante",
                    "title": "Ifood *Ifood",
                    "amount": 53.9
                }, {
                    "date": "2020-12-10",
                    "category": "servicos",
                    "title": "Netflix.Com",
                    "amount": 32.9
                }]
            }
        }
    )
}

Report1_response = {
    "200": openapi.Response(
        description="OK",
        examples={
            "application/json": {
                "histogram": {
                    "202101": {
                        "ranking": [{
                            "category": "supermercado",
                            "amount": 129.28
                        }, {
                            "category": "restaurante",
                            "amount": 156.7
                        }, {
                            "category": "transporte",
                            "amount": 304.73
                        }]
                    },
                    "202102": {
                        "ranking": [{
                            "category": "restaurante",
                            "amount": 181.7
                        }, {
                            "category": "transporte",
                            "amount": 451.44
                        }, {
                            "category": "outros",
                            "amount": 667.99
                        }]
                    }
                }
            }
        }
    )
}

Report2_response = {
    "200": openapi.Response(
        description="OK",
        examples={
            "application/json": {
                "histogram": {
                    "202101": {
                        "ranking": [{
                            "place": "Tembici",
                            "amount": 238.8
                        }, {
                            "place": "Supermercado Zona Sul",
                            "amount": 331.57
                        }]
                    },
                    "202102": {
                        "ranking": [{
                            "place": "Supermercado Zona Sul",
                            "amount": 185.32
                        }, {
                            "place": "Extra Super Lj",
                            "amount": 322.64
                        }]
                    }
                }
            }
        }
    )
}

Report3_response = {
    "200": openapi.Response(
        description="OK",
        examples={
            "application/json": {
                "histogram": {
                    "202101": 2250.08,
                    "202110": 2521.79,
                    "202105": 2525.23,
                }
            }
        }
    )
}

Report4_response = {
    "200": openapi.Response(
        description="OK",
        examples={
            "application/json": {
                "histogram": {
                    "202101": {
                        "ranking": [{
                            "place": "Uberbr Uber * Pending",
                            "count": 5
                        }, {
                            "place": "Uberbr Uber *Trip Help",
                            "count": 4
                        }, {
                            "place": "Ifood *Ifood",
                            "count": 3
                        }]
                    },
                    "202102": {
                        "ranking": [{
                            "place": "Ifood *Ifood",
                            "count": 7
                        }, {
                            "place": "Uberbr Uber * Pending",
                            "count": 5
                        }, {
                            "place": "Uber * Pending",
                            "count": 4
                        }]
                    }
                }
            }
        }
    )
}
