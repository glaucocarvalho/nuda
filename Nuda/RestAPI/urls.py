from django.template.defaulttags import url
from django.urls import path, re_path
from RestAPI import views
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions


schema_view = get_schema_view(
   openapi.Info(
      title="Snippets API",
      default_version='v1',
      description="Test description",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="contact@snippets.local"),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=[permissions.AllowAny],
)


urlpatterns = [
    path('histogram/', views.GetHistogram.as_view(), name='histogram'),
    path('bills/<str:bill_date>', views.GetBillsFromDate.as_view(), name='bills'),
    path('report1/<int:mode>', views.Report1.as_view(), name='report 1'),
    path('report2/', views.Report2.as_view(), name='report 2'),
    path('report3/', views.Report3.as_view(), name='report 3'),
    path('report4/', views.Report4.as_view(), name='report 4'),
    re_path(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    re_path(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    re_path(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
]
