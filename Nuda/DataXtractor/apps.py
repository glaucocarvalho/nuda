from django.apps import AppConfig


class DataxtractorConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'DataXtractor'
