from django.test import TestCase
from .models import Bill


class BillTestCase(TestCase):
    def test_line_input(self):
        self.assertEqual(len(Bill.objects.all()), 0)

        line = '2020-12-10,serviços,Netflix.Com,32.9'
        filename = 'nubank-2021-01.csv'
        Bill.save_line(line, filename, 2)
        bill = Bill.objects.get(id=1)

        self.assertEqual(len(Bill.objects.all()), 1)
        self.assertEqual(bill.date.strftime('%Y-%m-%d'), '2020-12-10')
        self.assertEqual(bill.category, 'serviços')
        self.assertEqual(bill.title, 'Netflix.Com')
        self.assertEqual(bill.amount, 32.9)
        self.assertEqual(bill.bill_date, '202101')
        self.assertEqual(bill.line_index, 2)

    def test_flush_bill_date(self):
        line = '2020-11-10,serviços,Netflix.Com,32.9'
        filename = 'nubank-2020-12.csv'
        Bill.save_line(line, filename, 1)

        line = '2020-12-10,serviços,Amazon.Com,33.39'
        filename = 'nubank-2021-01.csv'
        Bill.save_line(line, filename, 2)

        line = '2020-12-10,serviços,Mercado Li*,332.9'
        filename = 'nubank-2021-01.csv'
        Bill.save_line(line, filename, 3)

        self.assertEqual(len(Bill.objects.all()), 3)
        Bill.flush_bill_date("somefile-2021-01.csv")
        self.assertEqual(len(Bill.objects.all()), 1)
