import re
from os import walk
from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from DataXtractor.models import InvoiceHistory, Bill

FILE_STATUS = {
    -1: "Unexpected file status",
    1: "File not extracted yet, extracting...",
    2: "File already extracted, skipping...",
    3: "File with incomplete extraction, will try again...",
}


def get_files():
    files = []
    for (dirpath, dirname, filenames) in walk(settings.PATH_TO_FILES):
        for file in filenames:
            if re.search('.csv$', file):
                files.append(file)
    return files


def check_file(file):
    """
    Checks if file has already been extracted
    :param file:
    :return:
    """
    register = InvoiceHistory.get_file(file)
    if len(register) == 0:
        InvoiceHistory.save_file(file)
        return 1
    elif register[0].is_extracted:
        return 2
    elif not register[0].is_extracted:
        return 3
    return -1


def set_extraction_complete(file):
    register = InvoiceHistory.get_file(file)[0]
    register.is_extracted = True
    register.save()


def save_file(file):
    f = open(settings.PATH_TO_FILES + "/" + file, "r")
    i = 0
    for line in f:
        if i > 0:  # avoid extract the header content
            Bill.save_line(line, file, i)
        i += 1
    return 1


class Command(BaseCommand):
    help = 'This command extract data from files'

    def handle(self, *args, **options):
        self.stdout.write("Accessing invoice's files...")
        files = get_files()
        for file in files:
            self.stdout.write("Checking file " + file)
            file_status = check_file(file)
            self.stdout.write(FILE_STATUS[file_status])
            if file_status == 1 or file_status == 3:
                Bill.flush_bill_date(file)
                save_status = save_file(file)
                if save_status == 1:
                    set_extraction_complete(file)
            else:
                pass
        self.stdout.write("Task complete")
