from django.core.management import BaseCommand
from Pandaren import andy_panda


class Command(BaseCommand):
    help = 'This command process the data'

    def handle(self, *args, **options):
        andy_panda.main()
