from django.apps import AppConfig


class PandarenConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'Pandaren'
